from __future__ import absolute_import

from django.conf.urls import patterns, url

from .views import netflix_view

urlpatterns = patterns(
    "",
    url(r'^$',
        netflix_view,
        name='netflix_view'),
    )

